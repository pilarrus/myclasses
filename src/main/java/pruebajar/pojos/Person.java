package pruebajar.pojos;

import com.bluu.sch.wsbase.enums.InputLogMode;
import pruebajar.Animals;

/**
 * POJO Person
 */
public class Person {

    private String name;
    private String surName;
    private int age;
    private String dni;
    private Enum pet;
    private Enum inputLogMode;

    public Person() {
    }

    public Person(String name, String surName, int age, String dni) {
        this.name = name;
        this.surName = surName;
        this.age = age;
        this.dni = dni;
        this.pet = Animals.cat;
        this.pet = InputLogMode.off;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
