package pruebajar;

public enum FilterOperator {
    EQ("="),
    NE("!="),
    INC("LIKE"),
    CONT("MEMBER OF"),
    ST("LIKE"),
    END("LIKE"),
    GT(">"),
    LT("<"),
    IN("IN"),
    NIN("NOT IN");

    private String sqlOperator;

    FilterOperator(String sqlOperator) {
        this.sqlOperator = sqlOperator;
    }

    public String getSqlOperator() {
        return sqlOperator;
    }
}
